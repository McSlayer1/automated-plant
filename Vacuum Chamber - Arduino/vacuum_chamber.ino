/*
 
 */
 #include <Adafruit_LiquidCrystal.h>
#include <math.h>

// constants won't change. They're used here to
// set pin numbers:
class Button {
    protected:
        int pin;
        int buttonState;
        int lastButtonState;
        unsigned long lastDebounceTime;
    
    public:
        // Constructor :)
        // Setters
        void setPin(int p) {
            pin = p;
        }

        void setButtonState(int bs) {
            buttonState = bs;
        }

        void setLastButtonState(int lbs) {
            lastButtonState = lbs;
        }

        void setLastDebounceTime(unsigned long ldbt) {
            lastDebounceTime = ldbt;
        }

        // Getters
        int getPin(void) {
            return pin;
        }

        int getButtonState(void) {
            return buttonState;
        }

        int getLastButtonState(void) {
            return lastButtonState;
        }

        unsigned long getLastDebounceTime(void) {
            return lastDebounceTime;
        }
};

class HourButton : public Button {};

class MinButton : public Button {};

class SecButton : public Button {};

class StartButton : public Button {};

class StopButton : public Button {};

class Pump {
  private:
    int pin;
    bool active;

  public:
    void setPin(int p) {
      pin = p;
    }

    void setActive(bool a) {
      active = a;
    }

    int getPin(void) {
      return pin;
    }

    bool isActive(void) {
      return active;
    }

};

Adafruit_LiquidCrystal lcd_1(0);

const int HOUR_PIN = 2;
const int MIN_PIN = 4;
const int SEC_PIN = 7;
const int START_PIN = 8;
const int STOP_PIN = 12;
const int PUMP_PIN = 5;
const int TEMPT_PIN = A0;

HourButton hourDat; 
MinButton minDat; 
SecButton secDat; 
StartButton startDat; 
StopButton stopDat; 
Pump pumpMain; 

const unsigned long HOUR = 3600000;
const unsigned long MIN = 60000;
const unsigned long SEC = 1000;
unsigned long TIMER = 0;
unsigned long seconds = 0;
unsigned long minutes = 0;
unsigned long hours = 0;
unsigned long timerCurrent = 0;
unsigned long timeDelta = 0;
unsigned long timerStart = 0;
unsigned long vacStart = 0;
bool doRun = false;
float tempt = 0.0;
//const int buttonPin = 2;    // the number of the pushbutton pin
const int ledPin = 13;      // the number of the LED pin

// Variables will change:

int pumpState;

// the following variables are unsigned long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

/* Function tags */
void detectButton(Button &butt);
float get_tempt(void);
float c_to_f(float c);
void display_timer(void);

void setup() {
  Serial.begin(9600);

  lcd_1.begin(16, 2);

  lcd_1.print("hello world");

  //pinMode(buttonPin, INPUT);
  pinMode(HOUR_PIN, INPUT);
  hourDat.setPin(HOUR_PIN);

  pinMode(MIN_PIN, INPUT);
  minDat.setPin(MIN_PIN);

  pinMode(SEC_PIN, INPUT);
  secDat.setPin(SEC_PIN);

  pinMode(START_PIN, INPUT);
  startDat.setPin(START_PIN);

  pinMode(STOP_PIN, INPUT);
  stopDat.setPin(STOP_PIN);

  pinMode(PUMP_PIN, OUTPUT);
  pumpMain.setPin(PUMP_PIN);

  pumpState = false;
}

void loop() {
  detectButton(hourDat);
  detectButton(minDat);
  detectButton(secDat);
  detectButton(startDat);
  detectButton(stopDat);
  
  tempt = get_tempt();
  
  //Serial.print("Temperature: ");
  //Serial.println(c_to_f(tempt));

  /*
  Serial.print("HOUR_PIN: ");
  Serial.println(digitalRead(HOUR_PIN));

  Serial.print("MIN_PIN: ");
  Serial.println(digitalRead(MIN_PIN));

  Serial.print("SEC_PIN: ");
  Serial.println(digitalRead(SEC_PIN));

  Serial.print("START_PIN: ");
  Serial.println(digitalRead(START_PIN));

  Serial.print("STOP_PIN: ");
  Serial.println(digitalRead(STOP_PIN));

  Serial.print("TIMER: ");
  Serial.println(TIMER);

  Serial.print("pumpState: ");
  Serial.println(pumpState);
  */
  if(millis() - vacStart >= TIMER) {
    pumpMain.setActive(false);
  }
  else if(c_to_f(tempt) >= 45) {
  	pumpMain.setActive(false);
  }

  if(pumpMain.isActive()) {
    digitalWrite(pumpMain.getPin(), HIGH);
    timerCurrent = millis();
  } else {
    digitalWrite(pumpMain.getPin(), LOW);
  }
  display_timer();
  

} // END loop()

void detectButton(Button &butt) {
  // read the state of the switch into a local variable:
  int reading = digitalRead(butt.getPin());

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != butt.getLastButtonState()) {
    // reset the debouncing timer
    butt.setLastDebounceTime(millis());
  }

  timeDelta = millis() - butt.getLastDebounceTime();
  if (timeDelta >= debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:
    if(reading != butt.getButtonState()) {
      	timerStart = millis();
        butt.setButtonState(reading);

        if(butt.getPin() == HOUR_PIN && butt.getButtonState() == HIGH) {
          TIMER += HOUR;
        }
        
        if(butt.getPin() == MIN_PIN && butt.getButtonState() == HIGH) {
          TIMER += MIN;
        }
        
        if(butt.getPin() == SEC_PIN && butt.getButtonState() == HIGH) {
          TIMER += SEC;
        }

        if(butt.getPin() == START_PIN && butt.getButtonState() == HIGH) {
          //pumpState = HIGH;
          vacStart = millis();
          pumpMain.setActive(true);
        }

        if(butt.getPin() == STOP_PIN && butt.getButtonState() == HIGH) {
          //pumpState = LOW;
          TIMER = 0;
          pumpMain.setActive(false);
        }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  butt.setLastButtonState(reading);
} // END detectButton()

float get_tempt(void) {
  static float voltage;
  voltage = (analogRead(TEMPT_PIN)/1024.0) * 5.0;
  
  return (voltage - 0.5) * 100;
} // END get_tempt()

float c_to_f(float c) {
    return (c * (9.0/5.0)) + 32;
} // END c_to_f()

void display_timer(void) {
 	// Display the TIMER
  	seconds = floor(TIMER / 1000);
  	minutes = floor(seconds / 60);
  	hours = floor(minutes / 60);
  
  	seconds = seconds % 60;
  	minutes = minutes % 60;
  
  	//hours = hours % 24;
    lcd_1.setCursor(0, 0);
    lcd_1.print("Timer: ");
  	//Serial.print("Timer Set: ");

    lcd_1.print(hours);
  	//Serial.print(hours);

    lcd_1.print(":");
  	//Serial.print(":");

    lcd_1.print(minutes);
  	//Serial.print(minutes);

    lcd_1.print(":");
  	//Serial.print(":");

    lcd_1.print(seconds);
  	//Serial.print(seconds);

  	//Serial.println();
  
  	// Display time remaining
  	if(TIMER == 0) { 
      //lcd_1.setCursor(0, 1);
      //lcd_1.print("R: 00:00:00");
      //Serial.print("Time Remaining: ");
      //Serial.println("00:00:00");
    
    } else {
      lcd_1.setCursor(0, 1);
      //lcd_1.print("           ");
      timeDelta = TIMER - (timerCurrent - timerStart);

      seconds = floor(timeDelta / 1000);
      minutes = floor(seconds / 60);
      hours = floor(minutes / 60);

      seconds = seconds % 60;
      minutes = minutes % 60;
	  //lcd_1.setCursor(0, 1);
      lcd_1.print("Remain: ");
      //Serial.print("Time Remaining: ");
      lcd_1.print(hours);
      //Serial.print(hours);

      lcd_1.print(":");
      //Serial.print(":");

      lcd_1.print(minutes);
      //Serial.print(minutes);

      lcd_1.print(":");
      //Serial.print(":");

      lcd_1.print(seconds);
      //Serial.print(seconds);

      //Serial.println();
    }
  //Serial.println();
  	
}