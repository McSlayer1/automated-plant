// C++ code
//
#include <Servo.h>
#include <string.h>
#include <Adafruit_NeoPixel.h> // Be sure to install this in Arduino IDE - NeoPixel LED strip
#include "Adafruit_seesaw.h" // Install in Arduino IDE - For soil sensor

/* NeoPixel definitions */
#define NUMPIXELS 12
#define PIN 2
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 100; // timing delay in milliseconds

byte redColor = 0;
byte greenColor = 0;
byte blueColor = 0;

/* Soil Sensor definitions */
Adafruit_seesaw soil_sensor;
float tempC; //= ss.getTemp();
uint16_t capread; //= ss.touchRead(0);

/* Global constants/variables */
const short pumpPin = 5;
const short bulbPin = 4;
const short MOIST_CODE = 0;
const short LIGHT_CODE = 1;
const short TEMPT_CODE = 2;
const short ARRMAX = 60;
int total = 0;
int average = 0;
const unsigned long dayCycleInterval = 68400000;
const unsigned long nightCycleInterval = 18000000;
const unsigned long waterDuration = 60000;
const unsigned long debugInterval = 5000;
short i;
short count;
unsigned long currentMillis;

/* Servo variables*/
const short servoPin = 9;
Servo myServo;
short angle;
short angleMax = 179;
short angleMin = 0;
bool servoWritten = false;

/* Water variables */
struct Moisture {
  bool watering;
  float moistArr[ARRMAX];
  float moistCurrent;
  float moistMax;
  float moistMin;
  float moistAvg;
  unsigned long lastWaterCycle;
  unsigned long waterCycleStart;
} MoistDat;
const short moistPin = A0;

/* Light variables */
struct Lighting {
  bool dayCycle = false;
  bool nightCycle = false;
  float lightArr[ARRMAX];
  float lightCurrent;
  float lightMax;
  float lightMin;
  float lightAvg;
  unsigned long startDayCycle;
  unsigned long startNightCycle;
} LightDat;
const short lightPin = A1;

/* Temperatur Variables */
const short temptPin = A2;
float temptArr[ARRMAX];
float tempt;
float temptMax;
float temptMin;
float temptAvg;

/* Function tags */
void print_results(void);
void serial_plotter(void);
void func_setup_arrs(void);
void func_fan_cycle(void);
uint16_t get_soil_moisture(void);
void func_water(void);
void func_water_cycle(void);
void func_light(void);
void func_light_cycle(void);
float get_tempt(void);
void func_tempt(void);
float get_avg(float* arr);
void changeLightColor(byte r, byte g, byte b);

/* "MAIN" */
void setup(void)
{
  if(!soil_sensor.begin(0x36)) {
    Serial.println("ERROR! seesaw not found!");
    while(1) delay(1);
  } else {
    Serial.print("Seesaw started! Version: ");
    Serial.println(soil_sensor.getVersion(), HEX);
  }
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pumpPin, OUTPUT);
  pinMode(bulbPin, OUTPUT);
  myServo.attach(servoPin);
  
  angle = 0;
  count = 0;

  func_setup_arrs();
} // End setup()

void loop()
{
  //digitalWrite(LED_BUILTIN, HIGH);
  //digitalWrite(bulbPin, HIGH);
  //digitalWrite(pumpPin, HIGH);
  //delay(500); // Wait for 1000 millisecond(s)
  //digitalWrite(LED_BUILTIN, LOW);
  //digitalWrite(bulbPin, LOW);
  //digitalWrite(pumpPin, LOW);
  //delay(500); // Wait for 1000 millisecond(s)
  
  func_water();
  func_light();
  func_tempt();
  func_fan_cycle();
  serial_plotter();
  
  //Serial.print("Angle: ");
  //Serial.println(angle);
  
  //Serial.println("");
  count += 1;
  if(count == ARRMAX) {
    count = 0;
  }

  delay(10);
  
}  // END loop()

void print_results(byte code, float val, float valMin, float valMax, float* valArr) {
  static char title[15];
  static char str1[] = "Moisture";
  static char str2[] = "Light";
  static char str3[] = "Temperature";
  switch (code) {
    case MOIST_CODE:
      strcat(title, str1);
      break;
    case LIGHT_CODE:
      strcat(title, str2);
      break;
    case TEMPT_CODE:
      strcat(title, str3);
      break;
  }
  
  Serial.print(title);
  Serial.print(": ");
  Serial.println(val);
  
  //moistArr[count] = moisture;
  Serial.print("\t");
  Serial.print(title);
  Serial.print(" Min: ");
  Serial.println(valMin);
  
  Serial.print("\t");
  Serial.print(title);
  Serial.print(" Max: ");
  Serial.println(valMax);
  
  Serial.print("\t");
  Serial.print(title);
  Serial.print(" Avg: ");
  static short valAvg;
  valAvg = get_avg(valArr);
  Serial.println(valAvg);
  strcpy(title, "");
  
} // END print_results()

void serial_plotter(void) {
  Serial.print("MoistureAVG:");
  Serial.print(get_avg(MoistDat.moistArr));

  Serial.print("\tLightAVG:");
  Serial.print(get_avg(LightDat.lightArr));

  Serial.print("\tTemperatureAVG:");
  Serial.print(get_avg(temptArr));

  Serial.print("\tPumpStatus:");
  Serial.println(MoistDat.watering);  
}

void func_setup_arrs(void) {
  if (!soil_sensor.begin(0x36)) {
    //Serial.println("ERROR! seesaw not found");
    while(1) delay(1);
  } else {
    //Serial.print("seesaw started! version: ");
    //Serial.println(soil_sensor.getVersion(), HEX);
  }
  
  MoistDat.moistCurrent = 0;
  MoistDat.moistMax = 0;
  MoistDat.moistMin = 4096;
  MoistDat.watering = false;
  
  for (i = 0; i < ARRMAX; i++) {
    MoistDat.moistArr[i] = get_soil_moisture();//analogRead(moistPin); 
  }
  
  LightDat.lightCurrent = 0;
  LightDat.lightMax = 0;
  LightDat.lightMin = 4096;
  LightDat.dayCycle = true;
  LightDat.nightCycle = false;
  LightDat.startDayCycle = millis();
  LightDat.startNightCycle = millis();
  for (i = 0; i < ARRMAX; i++) {
    LightDat.lightArr[i] = analogRead(lightPin); 
  }
  
  tempt = 0;
  temptMax = 0;
  temptMin = 4096;
  for (i = 0; i < ARRMAX; i++) {
    temptArr[i] = get_tempt();
  }
} // END func_setup_arrs()

void func_fan_cycle(void) {
  // change the fan every so often
  if(count % 60 == 0) {
    servoWritten = false;
    switch(angle) {
      case 0:
        angle = 90;
        break;
      case 89:
        angle = 0;
        break;
      case 90:
        angle = 179;
        break;
      case 179:
        angle = 89;
        break;
    }
  }

  //angle = map(moisture, moistMin, moistMax, angleMin, angleMax);
  if(!servoWritten) {
    myServo.write(angle);
    servoWritten = true;
  }
} // END func_fan_cycle()

uint16_t get_soil_moisture(void) {
  return soil_sensor.touchRead(0);
}

void func_water(void) {
  MoistDat.moistCurrent = get_soil_moisture();
  //MoistDat.moistCurrent = analogRead(moistPin);
  if(MoistDat.moistCurrent > MoistDat.moistMax) {
    MoistDat.moistMax = MoistDat.moistCurrent;
  }
  if(MoistDat.moistCurrent < MoistDat.moistMin) {
    MoistDat.moistMin = MoistDat.moistCurrent;
  }
  
  MoistDat.moistArr[count] = MoistDat.moistCurrent;
  
  //print_results(MOIST_CODE, MoistDat.moistCurrent, MoistDat.moistMin, MoistDat.moistMax, MoistDat.moistArr);
  
  func_water_cycle();
} // END func_water()

void func_water_cycle(void) {
  // Check if the plant is getting dry
  if(get_avg(MoistDat.moistArr) <= 684 && !MoistDat.watering) {
    // We dry! turn on water pump, water for waterDuration
    MoistDat.watering = true;
    MoistDat.waterCycleStart = millis();
  } else {
    // check water cycle is going
    // if the current time - the time we started is greater than or eqaul to our desired water time
    // AND our desired moisture is high enough
    // Turn off the pump
    if(MoistDat.watering) {
      if(millis() - MoistDat.waterCycleStart >= waterDuration && get_avg(MoistDat.moistArr) >= 800) {
        // turn off the water pump
        MoistDat.watering = false;
      }
    }
    
  }

  // Handle turning on/off the pump when needed
  if(MoistDat.watering) {
    digitalWrite(pumpPin, HIGH);
  } else {
    digitalWrite(pumpPin, LOW);
  }
} // END void func_water_cycle()

void func_light(void){
  LightDat.lightCurrent = analogRead(lightPin);
  if(LightDat.lightCurrent > LightDat.lightMax) {
    LightDat.lightMax = LightDat.lightCurrent;
  }
  if(LightDat.lightCurrent < LightDat.lightMin) {
    LightDat.lightMin = LightDat.lightCurrent;
  }
  
  LightDat.lightArr[count] = LightDat.lightCurrent;
  
  //print_results(LIGHT_CODE, LightDat.lightCurrent, LightDat.lightMin, LightDat.lightMax, LightDat.lightArr);

  func_light_cycle();
} // END func_light

void func_light_cycle(void) {
  // IF our current time - the start of day cycle
  // is GREATER THAN OR EAQUAL to how long we want it to be day time
  // Make it night time
  if(millis() - LightDat.startDayCycle >= dayCycleInterval && LightDat.dayCycle) {
    // Make it the night cycle
    LightDat.dayCycle = false;
    LightDat.nightCycle = true;
    LightDat.startNightCycle = millis();

    // change lights from purple/white to blue - TO DO
    // rgb(0,0,255)
  }
  // IF our current time - the start of night cycle
  // is GREATER THAN OR EAQUAL to how long we want it to be night time
  // Make it day time
  if(millis() - LightDat.startNightCycle >= nightCycleInterval && LightDat.nightCycle) {
    // make it the day cycle
    LightDat.nightCycle = false;
    LightDat.dayCycle = true;
    LightDat.startDayCycle = millis();

    // change the lights from blue to purple/white - TO DO
    //rgb(255, 128, 255)
  }
  
  if(LightDat.dayCycle && !LightDat.nightCycle) {
    changeLightColor(255, 128, 255);    
  } else {
    changeLightColor(0, 0, 255);
  }
} // END func_light_cycle

float get_tempt(void) {
  static float voltage;
  voltage = (analogRead(temptPin)/1024.0) * 5.0;
  
  return (voltage - 0.5) * 100;
} // END get_tempt()

void func_tempt(void) {
  tempt = get_tempt();
  if(tempt > temptMax) {
    temptMax = tempt;
  }
  if(tempt < temptMin) {
    temptMin = tempt;
  }
  
  temptArr[count] = tempt;
  
  //print_results(TEMPT_CODE, tempt, temptMin, temptMax, temptArr);
} // END func_tempt()

float get_avg(float* arr) {
  static float temp;
  
  temp = 0;
  for(i = 0; i < ARRMAX; i++) {
    temp += arr[i];
  }
  
  return (temp / ARRMAX);
} // END get_avg()

void changeLightColor(byte r, byte g, byte b) {
  
  for (int i=0; i < NUMPIXELS; i++) {
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(r, g, b));

    // This sends the updated pixel color to the hardware.
    pixels.show();

    // Delay for a period of time (in milliseconds).
    delay(1);
  }
  
} // END changeLightColor()
